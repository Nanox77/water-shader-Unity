Simple shader which modify vertex and fragment to move Y axis and shift color of texture. The result is water in movement and ocilation in a Unity plane.

![Simple water with shader](Screenshot/screenshot.png)