﻿Shader "ShaderWorkshop/SimpleShader"                                    //Nombre del shader con nomenclatura Categoria/Nombre
{
	Properties                                                          //Variables publicas
	{
		_MainTex ("Texture", 2D) = "white" {}                           //declaracion de variable publica  _nombre ("Nombre en el editor", tipo) = valorDefault
		_SecondTex ("SecondTex", 2D) = "white" {}                           //declaracion de variable publica  _nombre ("Nombre en el editor", tipo) = valorDefault
		_Amplitude ("Amplitude",float) = 1
		_Speed ("Speed",float) = 200
	}
	SubShader
	{
		Tags {                                                          //Informacion adicional para el sistema de renderizado de UNITY
		    "Queue" = "Geometry"                                        //orden de renderizado -> Background, Geometry, Transparent, Overlay
		    "RenderType"="Opaque"                                       //tipo de renderizado
		}
		LOD 100

		Pass
		{
			CGPROGRAM                                                   //Inicio del shader
			#pragma vertex vertexShader                                 //Directiva para que se compile la funcion vertexShader
			#pragma fragment fragmentShader                             //Directiva para que se compile la funcion fragmentShader
			
			#include "UnityCG.cginc"                                    //include con todo el codigo necesario para que los shaders funcionen.

			struct appdata                                              //input de datos para el vertex shader
			{
				float4 vertex : POSITION;                               //position del vertice en coordenadas de objeto (object view)
				float2 uv : TEXCOORD0;                                  //coordenada de textura del vertice
			};

			struct vertex2fragment                                      //input de datos para el fragment shader
			{
				float2 uv : TEXCOORD0;                                  //coordenada de textura del fragment. (por pixel)
				float4 vertex : SV_POSITION;                            //position dentro del clipping space del fragment (pixel)
			};

            float _Amplitude;
            float _Speed;
			sampler2D _MainTex;                                         //declaracion de variable privada que corresponde con la variable publica
            sampler2D _SecondTex;
            float4 _SecondTex_ST;
			float4 _MainTex_ST;
			
			vertex2fragment vertexShader (appdata input)
			{
				vertex2fragment output;                                                 //creo el struct de salida
				output.vertex = UnityObjectToClipPos(input.vertex);                     //convierto el vertice de coordenadas de objeto a clipping space.
				float4 noise = tex2Dlod(_SecondTex, float4(input.uv.xy, 0, 0));         //cargo segunda textura
                output.vertex.y += _Amplitude * sin(_Time.x  * noise.x * _Speed);       //asigno y sumo a Y el seno del tiempo * la velocidad * la amplitude.
				output.uv = TRANSFORM_TEX(input.uv, _MainTex);                          //asigno la coordenada de textura correspondiente al vertice.
				return output;
			}
			
			fixed4 fragmentShader (vertex2fragment input) : SV_Target
			{
			    float2 uv = input.uv + _Time.x;
				fixed4 output = tex2D(_MainTex, uv);                    //asigno el pixel correspondiente de la textura
				return output;                                          //devuelvo el pixel de salida ya procesado.
			}
			ENDCG                                                       //fin del shader
		}
	}
}
